const express = require('express');
const Profile = require('../../models/leblon');
const User = require('../../models/envios');
const { check, validationResult } = require('express-validator');
const router = express.Router();
const auth = require('../../middleaware/auth')


router.get('/:userId', auth, async (req, res, next) => {
  try {
    const id = req.params.userId
    const profile = await Profile.findOne({user : id})
    if (profile) {
      res.json(profile)
    } else {
      res.status(404).send({ "error": "user not found" })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": "Server Error" })
  }
})



router.get('/me/', auth, async (req, res, next) => {
try {
  const id = req.user.id
  console.log(id)
  const profile = await Profile.findOne({user : id})
  if (profile) {
    res.json(profile)
  } else {
    res.status(404).send({ "error": "user not found" })
  }
} catch (err) {
  console.error(err.message)
  res.status(500).send({ "error": "Server Error" })
}
})

router.get('/',auth, async(req, res, next)=> {
  try{
    const user = await Profile.find({})
    res.json(user)
  }catch(err){
    console.error(err.message)
    res.status(500).send({"error" : "Server Error"})
  }
})

router.post('/', [], async (req, res, next) => {
  let {pago, mes, dia, condominio, net, luz, mae, total } = req.body
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() })
  } else {
      let profile = new Profile({pago, mes, dia, condominio, net, luz, mae, total })

      await profile.save()
      if (profile) {
        res.json(profile);
      }
    } 
  } )

  router.patch('/:userId', [], async (request, res, next) => {
    try {
      const errors = validationResult(request)
      if (!errors.isEmpty()) {
        res.status(400).send({ errors: errors.array() })
        return
      }
      const id = request.params.userId
      let bodyRequest = request.body
      const update = { $set: bodyRequest }
      const user = await Profile.findByIdAndUpdate(id, update, { new: true })
      if (user) {
        res.send(user)
      } else {
        res.status(404).send({ error: "User doesn't exist" })
      }
    } catch (err) {
      console.error(err.message)
      res.status(500).send({ "error": "Server Error" })
    }
  })

router.delete('/:userId', async(req, res, next) => {
  try {
    const id = req.params.userId
    const user = await Profile.findOneAndDelete({mes : id})
    if (user) {
      res.json(user)
    } else {
      res.status(404).send({ "error": "user not found" })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": "Server Error" })
  }
})

module.exports = router;