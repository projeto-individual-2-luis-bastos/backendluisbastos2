const express = require('express');
const Conta = require('../../models/envios');
const Profile = require('../../models/barra');
const Profilee = require('../../models/cartao');
const Profileee = require('../../models/geral');
const Profileeee = require('../../models/leblon');
const { check, validationResult } = require('express-validator');
const router = express.Router();
const bcrypt = require('bcryptjs');
const auth = require('../../middleaware/auth')

router.get('/',auth, async(req, res, next)=> {
  try{
    const user = await Conta.find({})
    res.json(user)
  }catch(err){
    console.error(err.message)
    res.status(500).send({"error" : "Server Error"})
  }
})

router.get('/:userId', [], async (req, res, next) => {
  try {
    const id = req.params.userId
    const user = await Conta.findOne({_id : id})
    if (user) {
      res.json(user)
    } else {
      res.status(404).send({ "error": "user not found" })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": "Server Error" })
  }
})

router.delete('/:userId', async(req, res, next) => {
  try {
    const id = req.params.userId
    const user = await Conta.findOneAndDelete({_id : id})
    await Profile.findOneAndDelete({user : id})
    await Profilee.findOneAndDelete({user : id})
    await Profileee.findOneAndDelete({user : id})
    await Profileeee.findOneAndDelete({user : id})
    await Profileeeee.findOneAndDelete({user : id})
    await Profileeeeee.findOneAndDelete({user : id})
    if (user) {
      res.json(user)
    } else {
      res.status(404).send({ "error": "user not found" })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": "Server Error" })
  }
})
router.put('/:userId', [
  check('email', 'email is not valid').isEmail(),
  check('nome').not().isEmpty(),
  check('senha', 'Please enter a password with 6 or more characters').isLength({ min: 6 })
], async (req, res, next) => {
  try {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    }
    const id = req.params.userId
    let { nome, email, senha, is_active, is_admin } = req.body
    let update = { nome, email, senha, is_active, is_admin };

    const salt = await bcrypt.genSalt(10);
    update.senha = await bcrypt.hash(senha, salt);

    let user = await Conta.findOneAndReplace({_id : id}, update, { new: true })
    if (user) {
      res.json(user)
    } else {
      res.status(404).send({ "error": "user not found" })
    }

  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": "Server Error" })
  }
})

router.patch('/:userId', [], async (request, res, next) => {
  try {
    const errors = validationResult(request)
    if (!errors.isEmpty()) {
      res.status(400).send({ errors: errors.array() })
      return
    }
    const id = request.params.userId
    const salt = await bcrypt.genSalt(10)
    
    let bodyRequest = request.body

    if(bodyRequest.senha){
      bodyRequest.senha = await bcrypt.hash(bodyRequest.senha, salt)
    }
    const update = { $set: bodyRequest }
    const user = await Conta.findByIdAndUpdate(id, update, { new: true })
    if (user) {
      res.send(user)
    } else {
      res.status(404).send({ error: "User doesn't exist" })
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": "Server Error" })
  }
})

router.post('/', [
  check('email', 'email is not valid').isEmail(),
  check('nome').not().isEmpty()
], async (req, res, next) => {
  try {
    let { nome, email, senha, is_active, is_admin } = req.body

    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() })
    } else {
      let usuario = new Conta({ nome, email, senha, is_active, is_admin })

      const salt = await bcrypt.genSalt(10);
      usuario.senha = await bcrypt.hash(senha, salt);

      await usuario.save()
      if (usuario.id) {
        res.json(usuario);
      }
    }
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ "error": "Server Error" })
  }
})

module.exports = router;