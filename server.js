const express = require('express')
var bodyParser = require('body-parser')
const connectDB = require('./config/db')
const path = require('path')
var cors = require('cors')
const app = express()
const PORT = process.env.PORT || 3001

app.use(cors())
app.use(express.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

connectDB()

app.use (function (req, res, next) {
    var schema = (req.headers['x-forwarded-proto'] || '').toLowerCase();
    if (req.headers.host.indexOf('localhost') < 0 && schema !== 'https') {
        res.redirect('https://' + req.headers.host + req.url);
    }
    next();
});

app.use('/envios', require('./routes/api/envios'))
app.use('/leblon', require('./routes/api/leblon'))
app.use('/barra', require('./routes/api/barra'))
app.use('/cartao', require('./routes/api/cartao'))
app.use('/geral', require('./routes/api/geral'))
app.use('/auth', require('./routes/api/auth'))
app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname))
})


app.listen(PORT, () => { console.log(`port ${PORT}`) })