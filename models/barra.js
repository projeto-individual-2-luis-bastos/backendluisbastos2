const mongoose = require('mongoose');

const ProfileSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    pago: {
        type: String
    },
    mes: {
        type: String
    },
    dia: {
        type: Number
    },
    condominio: {
        type: Number
    },
    net: {
        type: Number
    },
    luz: {
        type: Number
    },
    gas: {
        type: Number
    },
    total: {
        type: Number
    },
    date: {
      type: Date,
      default: Date.now
    },
}, { autoCreate : true })

module.exports = mongoose.model('barra', ProfileSchema);