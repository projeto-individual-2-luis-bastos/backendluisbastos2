const mongoose = require('mongoose');

const ProfileSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    pago: {
        type: String
    },
    mes: {
        type: String
    },
    dia: {
        type: String
    },
    biel: {
        type: Number
    },
    duda: {
        type: Number
    },
    empregada: {
        type: Number
    },
    esocial: {
        type: Number
    },
    abrale: {
        type: Number
    },
    total: {
      type: Number
    },
    date: {
      type: Date,
      default: Date.now
    },
}, { autoCreate : true })

module.exports = mongoose.model('geral', ProfileSchema);