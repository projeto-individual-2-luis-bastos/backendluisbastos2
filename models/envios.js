const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');

const ContaSchema = new mongoose.Schema({
    nome : {
        type : String,
        required : true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    senha :{
        type: String,
        required: true,
        select: false
    },
    is_active: {
        type: Boolean,
        default: true
    },
    is_admin: {
        type: Boolean,
        default: true
    },
    date: {
      type: Date,
      default: Date.now
    }
})

module.exports = mongoose.model('envios', ContaSchema);