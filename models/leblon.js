const mongoose = require('mongoose');

const ProfileSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    pago: {
        type: String
    },
    mes: {
        type: String
    },
    dia: {
        type: String
    },
    condominio: {
        type: Number
    },
    net: {
        type: Number
    },
    luz: {
        type: Number
    },
    mae: {
        type: Number
    },
    total: {
      type: Number
    },
    date: {
      type: Date,
      default: Date.now
    },
}, { autoCreate : true })

module.exports = mongoose.model('leblon', ProfileSchema);