const jwt = require('jsonwebtoken');
const config = require('config');


module.exports = function (req, res, next) {
  // Get token from header
  const token = req.header('x-auth-token');

  if(!token){
    return res.status(401).json({ msg: 'No token, authorization denied' });
  }

  try{
      jwt.verify(token, config.get('jwtSecret'), (error, decoded) => {
        if (error) {
          return res.status(401).json({ msg: 'Token is not valid' });
        } 
          req.user = decoded.user;
          if (req.baseUrl == '/leblon' && decoded.user.is_admin == false){
            return res.status(403).json({ msg: 'user is not admin' });
          }
          else if (req.baseUrl == '/barra' && decoded.user.is_admin == false){
            return res.status(403).json({ msg: 'user is not admin' });
          }
          next();
        
      })

  }catch (err) {
    console.error('something wrong with auth middleware');
    res.status(500).json({ msg: 'Server Error' });
  }
}